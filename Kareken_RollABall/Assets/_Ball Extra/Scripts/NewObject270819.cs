﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewObject270819 : MonoBehaviour
{
    public GameObject ballPrefab;
    private GameObject newBall;


    void Awake()
    {

        ballPrefab.transform.GetComponent<SphereCollider>().enabled = false;
        InvokeRepeating("MakeNewObjects", 0f, 2f);
    }

    private void MakeNewObjects()
    {
        
        newBall = Instantiate(ballPrefab, transform.position, Quaternion.identity);
        newBall.transform.GetComponent<SphereCollider>().enabled = true;
        newBall.transform.GetComponent<Rigidbody>().AddTorque(Random.onUnitSphere *20f);
        Destroy(newBall, 25f);//Destroy the ball after some amount of time.
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBMovePos090219 : MonoBehaviour
{
    private Rigidbody rb;
    Vector3 startPos, endPos;
    float lerp;

    public Vector3 direction = new Vector3(3f, 0f, 0f);
    
    
    void Start()
    {
        rb = transform.GetComponent<Rigidbody>();
        startPos = transform.position;
        endPos = startPos + direction;
    }

    
    void FixedUpdate()
    {
        lerp = Mathf.PingPong(Time.time, 1f);
        rb.MovePosition(Vector3.Lerp(startPos, endPos, lerp));
    }
}

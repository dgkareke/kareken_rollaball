﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//The following ensures there will be a rigidbody on the object
[RequireComponent(typeof(Rigidbody))]

public class RBCenterOfMass : MonoBehaviour
{

    void Start()
    {
        transform.GetComponent<Rigidbody>().centerOfMass = (Random.insideUnitSphere * .5f);
    }


}

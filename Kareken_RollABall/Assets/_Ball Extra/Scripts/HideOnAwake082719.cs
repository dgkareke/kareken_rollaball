﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideOnAwake082719 : MonoBehaviour
{
    /* 
     * This script disables the gameObject when the game starts
     * You can use it to hide objects placed to help during develoment
     * If they really shouldn't show in the final product, you 
     * should remove them before building the product
     */
    void Awake()
    {
        gameObject.SetActive(false);
    }

   
}

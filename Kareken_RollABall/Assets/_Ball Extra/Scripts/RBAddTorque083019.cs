﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The following ensures there will be a rigidbody on the object
[RequireComponent(typeof(Rigidbody))]

public class RBAddTorque083019 : MonoBehaviour
{
    public float howMuchTorque = 20f;

    void Start()
    {
        transform.GetComponent<Rigidbody>().AddTorque(Random.onUnitSphere * howMuchTorque);
    }
}

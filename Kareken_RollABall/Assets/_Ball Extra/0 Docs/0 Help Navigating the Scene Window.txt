alt (PC) opn (Mac)/Left Mouse to pan, scroll wheel to zoom 
Hold scroll wheel down to drag .

Right Mouse down and WASD to move around as in a game
Hot Keys
  ctl (Win) cmd(Mac) p causes the game to play or stop playing
When an object is selected in the scene windo
  F – will “find” the object by moving the scene camera to it
  W – will cause handles to appear so you can drag the object
  E – allows you to rotate the object
  R – allows you to scale it
The Gizmo in the upper right of the scene view:
   Click one of the axis to view the world from that direction
   Click the center box to go from perspective to orthographic view
To allign with the main camera, select the camera, then go to
    GameObject/Align View To Selected.


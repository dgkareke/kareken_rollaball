﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JelloBox_092019 : MonoBehaviour
{
    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.gameObject.GetComponent<Rigidbody>())
        {
            otherObject.gameObject.GetComponent<Rigidbody>().drag = 10f;

        }
    }
    void OnTriggerExit(Collider otherObject)
    {
        if (otherObject.gameObject.GetComponent<Rigidbody>())
        {
            otherObject.gameObject.GetComponent<Rigidbody>().drag = .3f;

        }
    }
}

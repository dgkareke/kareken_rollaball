﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeObjects_091919 : MonoBehaviour
{
    public GameObject thePrefab;
    private GameObject newPrefab;

    public float rateInSeconds = 2f;
    public float delayBeforeFirst = 0f;
    public float prefabLifetime = 25f;

    void Start()
    {
        //This will repeatedly call the named function after a delay of rateInSEconds
        InvokeRepeating("MakeNewObjects", delayBeforeFirst, rateInSeconds);
    }

    private void MakeNewObjects()
    {
        //Create the new object
        newPrefab = Instantiate(thePrefab, transform.position, Quaternion.identity);
        //Make it a child of this object
        newPrefab.transform.parent = transform;
        //Destroy it in a few seconds
        Destroy(newPrefab, prefabLifetime);//Destroy the ball after the number of seconds in prefabLifetime.
    }


}

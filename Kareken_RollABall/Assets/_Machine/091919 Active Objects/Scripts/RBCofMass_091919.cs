﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class RBCofMass_091919 : MonoBehaviour
{
    void Start()
    {
        transform.GetComponent<Rigidbody>().centerOfMass = (Random.insideUnitSphere * .5f);
    }
}

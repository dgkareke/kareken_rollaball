﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelePort_091919 : MonoBehaviour
{
 /*
 * If this is attached to an object with a trigger and the public
 * variable destination has been supplied with another onject
 * (ideally with its collder diabled), anything that wanders 
 * into the first trigger will be transported to the position 
 * of destination.
 *
 * 
 */

    public Transform destination;
    void Start()
    {
        
        destination.GetComponent<Renderer>().enabled = false;
    }

    void OnTriggerEnter(Collider obj)
    {
        obj.transform.position = destination.position;
        obj.GetComponent<Rigidbody>().velocity *= 0f;

    }
}

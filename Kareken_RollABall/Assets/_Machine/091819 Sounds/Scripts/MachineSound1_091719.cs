﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineSound1_091719 : MonoBehaviour
{
    public AudioClip soundToPlay;

    void Start()
    {
        
    }


    void OnCollisionEnter(Collision theSurfaceIHit)
    {
        AudioSource.PlayClipAtPoint(soundToPlay, transform.position);
    }
}

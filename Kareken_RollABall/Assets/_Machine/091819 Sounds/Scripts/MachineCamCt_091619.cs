﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineCamCt_091619 : MonoBehaviour
{
    public GameObject Cam1, Cam2, Cam3, UICam;
    
    void Start()
    {
        Cam1On();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Cam1On();
        } else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Cam2On();
        } else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Cam3On();
        } else if (Input.GetKeyDown(KeyCode.U))
        {
            UICamToggle();
        }
    }

    void Cam1On()
    {
        Cam1.SetActive(true);
        Cam2.SetActive(false);
        Cam3.SetActive(false);
    }
    void Cam2On()
    {
        Cam1.SetActive(false);
        Cam2.SetActive(true);
        Cam3.SetActive(false);
    }
    void Cam3On()
    {
        Cam1.SetActive(false);
        Cam2.SetActive(false);
        Cam3.SetActive(true);
    }
    void UICamToggle()
    {
        if (UICam.activeSelf)
        {
            UICam.SetActive(false);
        }
        else
        {
            UICam.SetActive(true);
        }
    }
}

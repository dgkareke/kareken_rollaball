﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBAddTorque_091719 : MonoBehaviour
{
   
    void Start()
    {
        transform.GetComponent<Rigidbody>().AddTorque(Random.onUnitSphere * 20f);
    }

   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour {

    //public are editable in inspector
    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    bool onGround;
    public float jumpPower;

    public GameObject pickupPrefab;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";

        //optional: have them randomly spawn, but difficult to implement
        int pickupsSpawned = 10;

        /*while (pickupsSpawned > 0)
        {
            pickupsSpawned--;
            SpawnPickup();
        }*/
    }

    private void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);
        
        if (Input.GetButtonDown("Jump") && onGround)
        {
            Jump();
        }
    }

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

        rb.AddForce (movement * speed);
    }

    //adds 1 to count if touching collectable
    void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag ("Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
        }
    }

    //checks for collision with harmful objects
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.other.CompareTag("Enemy"))
        {
            gameObject.SetActive(false);
            Invoke("RestartLevel", 2f);
        }
    }

    //sets up and updates  UI: Count and Win Text
    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString ();
        if (count >= 10)
        {
            winText.text = "You Win!";
            //if (KeyCode == Space)
            //{
            Invoke("RestartLevel", 2f);
            //}
        }
    }

    //reloads level after player dies or all collectables are picked up
    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //jump function
    void Jump()
    {
        rb.AddForce(Vector3.up * jumpPower);
    }

    //helper function for spawning pickups randomly
    void SpawnPickup()
    {
        Vector3 randomPos = new Vector3(Random.Range(-8f, 8f), .5f, Random.Range(-8f, 8f));
        Instantiate(pickupPrefab, randomPos, pickupPrefab.transform.rotation);
    }
}
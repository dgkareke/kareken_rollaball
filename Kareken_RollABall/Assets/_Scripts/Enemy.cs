﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy : MonoBehaviour
{
    GameObject player;
    Rigidbody rb;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 dirToPlayer = player.transform.position - transform.position;
        dirToPlayer = dirToPlayer.normalized;
        rb.AddForce(dirToPlayer * speed);
    }
}
